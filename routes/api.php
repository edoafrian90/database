<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProdukCont;
use App\Http\Controllers\VoucherCont;
use App\Http\Controllers\CartCont;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::get('produk', [ProdukCont::class,"index"]);
// Route::post('produk', [ProdukCont::class,"store"]);
// Route::put('produk/{produk}/update', [ProdukCont::class,"update"]);
// Route::delete('produk{produk}/delete', [ProdukCont::class,"destroy"]);

Route::resource('produk',ProdukCont::class);



// Route::post('voucher', [VoucherCont::class,"index"]);
Route::resource('voucher',VoucherCont::class);
// Route::resource('voucher',VoucherCont2::class);
Route::resource('cart',CartCont::class);


Route::get('/testing', function (Request $request) {
    return 'hasil Testing';
});